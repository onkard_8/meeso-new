export interface Stock {
    id : number;
    ticker: string;
    volume: string;
    price: number;
    transactionType: string;
}

