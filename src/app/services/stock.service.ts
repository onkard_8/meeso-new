
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Stock } from '../model/stock';
import { HttpEvent } from '@angular/common/http/src/response';


@Injectable()
export class StockService {

  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  getStocksforUser() : Observable<Stock[]> {

    return this.http.get<Stock[]>('http://tradestocks-tradestocks.chennaidevops12.conygre.com/api/stocks/user');
  }


  //post
  createStock(stock: Stock): Observable<any> {
    return this.http.post('http://tradestocks-tradestocks.chennaidevops12.conygre.com/api/stocks/user', JSON.stringify(stock), this.httpOptions);
  }

  getStockfromAdmin(): Observable<Stock[]> {
    return this.http.get<Stock[]>('http://tradestocks-tradestocks.chennaidevops12.conygre.com/api/stocks/admin');
    
  }

}
