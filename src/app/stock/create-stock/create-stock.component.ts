import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Stock } from '../../model/stock';
import { StockService } from '../../services/stock.service';

@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
  styleUrls: ['./create-stock.component.css']
})
export class CreateStockComponent implements OnInit{
  [x: string]: any;
  public confirmed = false;
  public message = null;
  selectedStock : any = [];
  price;

  public types = [{name:'BUY'},{name:'SELL'}];
  public stocks;


  stocks2 : any = [];

  @Input() stockDetails = { id:0,ticker: '', volume: '', price:0, transactionType: '' }

  constructor(
    public stockService: StockService, 
    public router: Router
  ) { }


  ngOnInit(): void {
    this.getData();
    console.log(this.stocks2)
  }

  getData(){
    return this.stockService.getStockfromAdmin()
    .subscribe((data => { this.stocks2 = data;}) )
  }



  onChange(){
   // console.log(this.selectedStock);
    this.price=this.selectedStock;
  }

  addStock(transaction) {
    console.log(transaction)
    // this.stockService.createStock(this.stockDetails).subscribe((data: {}) => {
    //   this.router.navigate(['/stocks/list'])
      
   // })
  }


    
}
