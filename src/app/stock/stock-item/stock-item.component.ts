import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Stock } from '../../model/stock';
import { StockService } from '../../services/stock.service';

@Component({
  selector: 'app-stock-item',
  templateUrl: './stock-item.component.html',
  styleUrls: ['./stock-item.component.css']
})
export class StockItemComponent implements OnInit {


  stocks1 : any = [];
  constructor( 
    public stockService: StockService
    ) { }

  ngOnInit(): void {
    this.stockService.getStocksforUser()
    .subscribe(data => { this.stocks1 = data;} )
  }

}