import { Component, OnInit } from '@angular/core';
import { StockService } from '../../services/stock.service';
import { Stock } from '../../model/stock';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  
  stocks1 : any = [];
  constructor( 
    public stockService: StockService
    ) { }

  ngOnInit(): void {
    this.stockService.getStocksforUser()
    .subscribe(data => { this.stocks1 = data;} )
  }


}
